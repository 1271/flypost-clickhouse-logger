import asyncio
import logging
from pathlib import Path
from time import time
from typing import Dict, Any
from os import getenv

import yaml
from aiochclient import ChClient
from aiochclient.exceptions import ChClientError
from aiohttp import ClientSession
from fastapi import BackgroundTasks, FastAPI
from fastapi.responses import Response

logger = logging.getLogger("uvicorn.error")

cfg_path = Path(__file__).parent.absolute()
with cfg_path.joinpath('config.yml').open() as r:
    config = yaml.safe_load(r)

try:
    with cfg_path.joinpath('config.local.yml').open() as r:
        _cfg = yaml.safe_load(r)
    config.update(_cfg)
except FileNotFoundError:
    pass

table = config.get('backend_table')
disable_logs = {k: 0 for k in config.get('disable_logs', []) if str(k).startswith('log_')}  # type: Dict[str, int]
backend_url = config.get('backend_url')
backend_database = config.get('backend_database')
backend_user = config.get('backend_user', getenv('CLICKHOUSE_USER', None))
backend_password = config.get('backend_password', getenv('CLICKHOUSE_PASSWORD', None))

version = '1.1.0'

app = FastAPI(title='ClickHouse inserts queue', version=version)

logger.info(f'App version: {version}')
logger.info('Url: %s' % (backend_url,))
logger.info('Database: %s' % (backend_database,))
logger.info('User: %s' % (backend_user,))

if len(disable_logs) > 0:
    logger.info('Disabled logs: ( %s )' % (', '.join(disable_logs), ))


class Storage:
    to_push_data = []
    to_push_data_timer = None
    _fields = None
    _keys = None
    __connection = None

    def __init__(self, connection: ChClient):
        self.to_push_data_timer = time()
        self.__connection = connection

        self._fields = set(config.get('table_fields'))
        self._keys = ','.join(self._fields)

    async def send_data(self, data: dict):
        keys = set(data.keys())
        diff = self._fields ^ keys
        if len(diff & self._fields):
            logger.error(f'Missing keys: {", ".join(diff & self._fields)}')
            return
        if len(diff & keys):
            logger.error(f'Redundant keys: {", ".join(diff & keys)}')
            return

        self.to_push_data.append(data)

        # если прошло меньше 2 секунд, скипаем
        if time() - self.to_push_data_timer < 2:
            logger.info('Wait...')
            return

        # если нет записей, скипаем
        if len(self.to_push_data) > 0:
            self.to_push_data_timer = time()
            data = self.to_push_data

            logger.info('Before put rows')

            try:
                await self.__connection.execute(f'INSERT INTO {table} ({self._keys}) VALUES ', *(
                    tuple(values[key] for key in self._fields) for values in data
                ))
                self.to_push_data = []
            except ChClientError as e:
                logger.error(e)
                if len(self.to_push_data) > 10:
                    # Пытаемся удалять по 1 сообщению за раз, чтобы сохранить хоть что-то
                    logger.error('Clear "to_push_data" variable!')
                    logger.error(self.to_push_data.pop(0))


async def create_storage() -> Storage:
    options = {
        'url': backend_url,
        'database': backend_database,
        'password': backend_password,
        'user': backend_user,
    }

    options.update(disable_logs)

    connection = ChClient(ClientSession(), **options)

    if not await connection.is_alive():
        raise RuntimeError('Could not establish connection with clickhouse')

    return Storage(connection)


storage_cache = {'errors': 0, 'connection': None, 'latest_check': time()}


@app.on_event("startup")
async def setup():
    await get_storage()
    logger.info('Connected to db')


async def get_storage() -> Storage:
    latest_check = storage_cache['latest_check']
    time_now = time()

    if latest_check + 2 > time_now:
        await asyncio.sleep(2 - (time_now - latest_check))

    if storage_cache['errors'] > 5:
        raise Exception('Could not establish connection with clickhouse')
    connection = storage_cache.get('connection')

    try:
        storage_cache['latest_check'] = time_now

        if connection is None:
            storage_cache['connection'] = await create_storage()
        storage_cache['errors'] = 0
    except Exception as e:
        storage_cache['errors'] = storage_cache['errors'] + 1
        logger.error(e)
        await asyncio.sleep(2)
        return await get_storage()
    return storage_cache['connection']


@app.route('/', methods=['GET'])
async def index_page(req):
    return Response('Ok.')


@app.post('/')
async def generate(background_tasks: BackgroundTasks, data: Dict[str, Any]):
    connection = await get_storage()
    background_tasks.add_task(connection.send_data, data=data)
    return {
        'response': 'Ok'
    }
