# Clickhouse log proxy 

## Create database and table:

```clickhouse
CREATE DATABASE IF NOT EXISTS flypost;
CREATE TABLE IF NOT EXISTS flypost.flypost_log (
    url_pattern String,
    ip String,
    time Float64,
    duration Float32,
    memory UInt32,
    user_id String,
    user_agent String
) Engine = MergeTree
    PARTITION BY toUInt32(time / 864000)
    ORDER BY (time, url_pattern, user_id)
; -- partition over by 10 days  
```
